package java8;


import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;

public class Lambdas {

    @Test
    public void testComparator() {
        // Comparator的传统写法
        Comparator<String> comparator = new Comparator<String>() {
            public int compare(String s1, String s2) {
                return s1.length() - s2.length();
            }
        };

        // Comparator的Lambda格式的完整写法
        Comparator<String> lambdaComparator = (String s1, String s2) -> {
            return s1.length() - s2.length();
        };

        // Comparator的Lambda格式的类型感知写法
        lambdaComparator = (s1, s2) -> {
            return s1.length() - s2.length();
        };

        // Comparator的Lambda格式的最简写法
        lambdaComparator = (s1, s2) -> s1.length() - s2.length();

        List<String> writer = Arrays.asList("AA", "A", "AAAA", "AAA");
        System.out.println(writer);
        Collections.sort(writer, comparator);
        System.out.println(writer);

        writer = Arrays.asList("AA", "A", "AAAA", "AAA");
        System.out.println(writer);
        Collections.sort(writer, lambdaComparator);
        System.out.println(writer);
    }

    @Test
    public void testPredicate() {
        Predicate<String> predicate = (s) -> s.length() > 5;
        // 由于只有一个参数，所以可省略括号；
        // 表达式的结果类型与返回的类型一致，所以可以省略return。
        predicate = s -> s.length() > 5;

        System.out.println(predicate.test("Hello"));
        System.out.println(predicate.test("Hello, World!"));
    }

    @Test
    public void testRunnable() {
        Runnable runnable = () -> System.out.println("Hello, Runnable!");
        runnable.run();
    }

}
