package lambdasinaction.chap3;


public class Test {

public interface Adder {
    int add(int a, int b);
}

public interface SmartAdder extends Adder {
    int add(double a, double b);
}

public interface Nothing {
}

public void process(Runnable r){
    r.run();
}

public void testProcess() {
    process(() -> System.out.println("This is awesome!!"));
}

}


