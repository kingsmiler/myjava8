package java8;


@FunctionalInterface
public interface LambdaInterface {
    public void abstractMethod();

    public default void defaultMethod() {
        System.out.println("default method!");
    }

    public static void staticMetod() {
        System.out.println("static method!");
    }
}
