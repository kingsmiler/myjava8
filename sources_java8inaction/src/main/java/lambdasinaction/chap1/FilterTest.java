package lambdasinaction.chap1;

import java.io.File;
import java.io.FileFilter;

public class FilterTest {

    public File[] getHiddenFiels1() {
        File[] hiddenFiles = new File(".").listFiles(new FileFilter() {
            public boolean accept(File file) {
                return file.isHidden();
            }
        });

        return hiddenFiles;
    }

    public File[] getHiddenFiels2() {
        return new File(".").listFiles(File::isHidden);
    }
}
