package lambdasinaction.chap2;


public class ThreadTest {

    public static void main(String[] args) {
        Thread t = new Thread(() -> System.out.println("Hello world"));
        t.start();
    }

}
